﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NumberGenerator;
using System.Collections.Generic;

namespace NumberGenerator.Tests {

    [TestClass]
    public class GenerateRandomNumbersTests {

        [TestMethod]
        public void TestNumbersForGameConstructor_defaultValue_QuantityOfRandomNumbers() {
            GenerateRandomNumbers nums = new GenerateRandomNumbers();

            var expected = 4;
            var actual = nums.QuantityOfRandomNumbers;

            Assert.AreEqual(expected, actual, "QuantityOfRandomNumbers should equal 4");
        }

        [TestMethod]
        public void TestNumbersForGameConstructor_defaultValue_MinimumRandomNumber() {
            GenerateRandomNumbers nums = new GenerateRandomNumbers();

            var expected = 1;
            var actual = nums.MinimumRandomNumber;

            Assert.AreEqual(expected, actual, "MinimumRandomNumber should equal 1");
        }

        [TestMethod]
        public void TestNumbersForGameConstructor_defaultValue_MaxRandomNumber() {
            GenerateRandomNumbers nums = new GenerateRandomNumbers();

            var expected = 10;
            var actual = nums.MaximumRandomNumber;

            Assert.AreEqual(expected, actual, "MaximumRandomNumber should equal 10");
        }

        [TestMethod]
        public void TestNumbersForGameConstructor_defaultValue_MaximumNumberOfDuplicates() {
            GenerateRandomNumbers nums = new GenerateRandomNumbers();

            var expected = 2;
            var actual = nums.MaximumNumberOfDuplicates;

            Assert.AreEqual(expected, actual, "MaximumNumberOfDuplicates should equal 2");
        }

        [TestMethod]
        public void TestNumbersForGameConstructor_qtyofRandomNumbersParameter() {
            GenerateRandomNumbers nums = new GenerateRandomNumbers(5);

            Assert.AreEqual(5, nums.QuantityOfRandomNumbers, "QuantityOfRandomNumbers should equal 5");
            Assert.AreEqual(1, nums.MinimumRandomNumber, "MinimumRandomNumber should equal 1");
            Assert.AreEqual(10, nums.MaximumRandomNumber, "MaximumRandomNumber should equal 10");
            Assert.AreEqual(2, nums.MaximumNumberOfDuplicates, "MaximumNumberOfDuplicates should equal 2");
        }

        [TestMethod]
        public void TestNumbersForGameConstructor_qtyofRandomNumbersAndMinimumRandomNumberParameters_MinimumLessThanMaximum() {
            GenerateRandomNumbers nums = new GenerateRandomNumbers(5, 4);

            Assert.AreEqual(5, nums.QuantityOfRandomNumbers, "QuantityOfRandomNumbers should equal 5");
            Assert.AreEqual(4, nums.MinimumRandomNumber, "MinimumRandomNumber should equal 9");
            Assert.AreEqual(10, nums.MaximumRandomNumber, "MaximumRandomNumber should equal 10");
            Assert.AreEqual(2, nums.MaximumNumberOfDuplicates, "MaximumNumberOfDuplicates should equal 2");
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException), "Increase the maximumNumberOfDuplicates, inorder to generate the correct qtyofRandomNumbers")]
        public void TestNumbersForGameConstructor_qtyofRandomNumbersAndMinimumRandomNumberParameters_MinimumEqualMaximum() {
            // The (Max - Min + 1) * Dupplicates calculation does not 
            // allow Generator to reach Quantity of numbers desired.
            GenerateRandomNumbers nums = new GenerateRandomNumbers(5, 10);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException), "maximumRandomNumber can not be less than minimumRandomNumber")]
        public void TestNumbersForGameConstructor_qtyofRandomNumbersAndMinimumRandomNumberParameters_MinimumGreaterThanMaximum() {
            GenerateRandomNumbers nums = new GenerateRandomNumbers(5, 11);
        }

        [TestMethod]
        public void TestNumbersForGameConstructor_qtyofRandomNumbersAndMinimumRandomNumberAndMaximumRandomNumberParameters_MinimumLessThanMaximum() {
            GenerateRandomNumbers nums = new GenerateRandomNumbers(5, 5, 11);

            Assert.AreEqual(5, nums.QuantityOfRandomNumbers, "QuantityOfRandomNumbers should equal 5");
            Assert.AreEqual(5, nums.MinimumRandomNumber, "MinimumRandomNumber should equal 10");
            Assert.AreEqual(11, nums.MaximumRandomNumber, "MaximumRandomNumber should equal 11");
            Assert.AreEqual(2, nums.MaximumNumberOfDuplicates, "MaximumNumberOfDuplicates should equal 2");
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException), "Increase the maximumNumberOfDuplicates, inorder to generate the correct qtyofRandomNumbers")]
        public void TestNumbersForGameConstructor_qtyofRandomNumbersAndMinimumRandomNumberAndMaximumRandomNumberParameters_MinimumEqualMaximum() {
            // The (Max - Min + 1) * Dupplicates calculation does not 
            // allow Generator to reach Quantity of numbers desired.
            GenerateRandomNumbers nums = new GenerateRandomNumbers(5, 11, 11);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException), "maximumRandomNumber can not be less than minimumRandomNumber")]
        public void TestNumbersForGameConstructor_qtyofRandomNumbersAndMinimumRandomNumberAndMaximumRandomNumberParameters_MinimumGreaterThanMaximum() {
            GenerateRandomNumbers nums = new GenerateRandomNumbers(5, 12, 11);
        }

        [TestMethod]
        public void TestNumbersForGameConstructor_AllParameters_MinimumLessThanMaximum() {
            GenerateRandomNumbers nums = new GenerateRandomNumbers(5, 10, 11, 5);

            Assert.AreEqual(5, nums.QuantityOfRandomNumbers, "QuantityOfRandomNumbers should equal 5");
            Assert.AreEqual(10, nums.MinimumRandomNumber, "MinimumRandomNumber should equal 10");
            Assert.AreEqual(11, nums.MaximumRandomNumber, "MaximumRandomNumber should equal 11");
            Assert.AreEqual(5, nums.MaximumNumberOfDuplicates, "MaximumNumberOfDuplicates should equal 5");
        }

        [TestMethod]
        public void TestNumbersForGameConstructor_AllParameters_MinimumEqualMaximum() {
            GenerateRandomNumbers nums = new GenerateRandomNumbers(5, 11, 11, 5);

            Assert.AreEqual(5, nums.QuantityOfRandomNumbers, "QuantityOfRandomNumbers should equal 5");
            Assert.AreEqual(11, nums.MinimumRandomNumber, "MinimumRandomNumber should equal 11");
            Assert.AreEqual(11, nums.MaximumRandomNumber, "MaximumRandomNumber should equal 11");
            Assert.AreEqual(5, nums.MaximumNumberOfDuplicates, "MaximumNumberOfDuplicates should equal 5");
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException), "maximumRandomNumber can not be less than minimumRandomNumber")]
        public void TestNumbersForGameConstructor_AllParameters_MinimumGreaterThanMaximum() {
            GenerateRandomNumbers nums = new GenerateRandomNumbers(5, 12, 11, 5);
        }

        [TestMethod]
        public void TestGenerateRandomNumberCollection_Return20Numbers() {
            GenerateRandomNumbers nums = new GenerateRandomNumbers(20, 1, 10, 3);

            List<int> listOfNums = nums.GenerateRandomNumberCollection();

            var expected = 20;
            var actual = listOfNums.Count;


            Assert.AreEqual(expected, actual, "Should return 20 random numbers");
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException), "Increase the maximumNumberOfDuplicates, inorder to generate the correct qtyofRandomNumbers")]
        public void TestGenerateRandomNumberCollection_MinMaxAndDuplicates_NotEnoughNumbers() {
            // The (Max - Min + 1) * Dupplicates calculation does not 
            // allow Generator to reach Quantity of numbers desired.
            GenerateRandomNumbers nums = new GenerateRandomNumbers(20, 5, 10, 3);
        }

        [TestMethod]
        public void TestGenerateRandomNumberCollection_MinOfFive() {
            GenerateRandomNumbers nums = new GenerateRandomNumbers(20, 5, 11, 3);

            List<int> listOfNums = nums.GenerateRandomNumberCollection();

            listOfNums.Sort();

            Assert.IsFalse(listOfNums[0] < 5, "No numbers less than 5");
        }

        [TestMethod]
        public void TestGenerateRandomNumberCollection_MaxOfTwenty() {
            GenerateRandomNumbers nums = new GenerateRandomNumbers(20, 5, 20, 3);

            List<int> listOfNums = nums.GenerateRandomNumberCollection();

            listOfNums.Sort();

            Assert.IsFalse(listOfNums[listOfNums.Count - 1] > 20, "No numbers greater than 20");
        }
    }
}
