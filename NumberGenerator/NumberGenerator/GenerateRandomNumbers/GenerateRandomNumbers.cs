﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NumberGenerator {

    public class GenerateRandomNumbers : IRandomNumberGenerator {
        /// <summary>
        /// List to hold generated random integer collection
        /// </summary>
        public List<int> RandomNumbers { get; private set; }

        /// <summary>
        /// Quantity of how many random numbers you would like to generate
        /// </summary>
        public int QuantityOfRandomNumbers { get; set; }

        /// <summary>
        /// Minimum random number to generate
        /// </summary>
        public int MinimumRandomNumber { get; set; }
        public int MaximumRandomNumber { get; set; }
        public int MaximumNumberOfDuplicates { get; set; }

        /// <summary>
        /// Default Constructor will generate random number collection.
        /// Will generate four random numbers, 1 through 10 inclusive, with a maximum of 2 duplicates.
        /// </summary>
        public GenerateRandomNumbers() : this(4, 1, 10, 2) { }

        /// <summary>
        /// Default Constructor for generating random number collection.
        /// Will generate random numbers, 1 through 10 inclusive, with a maximum of 2 duplicates.
        /// </summary>
        /// <param name="qtyofRandomNumbers">Quantity of random numbers wanted</param>
        public GenerateRandomNumbers(int qtyofRandomNumbers) : this(qtyofRandomNumbers, 1, 10, 2) { }

        /// <summary>
        /// Default Constructor for generating random number collection.
        /// Will generate random numbers, minimum number given through 10 inclusive, with a maximum of 2 duplicates.
        /// </summary>
        /// <param name="qtyofRandomNumbers">Quantity of random numbers wanted</param>
        /// <param name="minimumRandomNumber">Minimum number for random numbers, must be less than or equal to 10 (maximum random number)</param>
        public GenerateRandomNumbers(int qtyofRandomNumbers, int minimumRandomNumber) : this(qtyofRandomNumbers, minimumRandomNumber, 10, 2) { }

        /// <summary>
        /// Default Constructor for generating random number collection.
        /// Will generate random numbers, minimum number given through maximum number given inclusive, with a maximum of 2 duplicates.
        /// </summary>
        /// <param name="qtyofRandomNumbers">Quantity of random numbers wanted</param>
        /// <param name="minimumRandomNumber">Minimum number for random numbers</param>
        /// <param name="maximumRandomNumber">Maximum number for random numbers</param>
        public GenerateRandomNumbers(int qtyofRandomNumbers, int minimumRandomNumber, int maximumRandomNumber) : this(qtyofRandomNumbers, minimumRandomNumber, maximumRandomNumber, 2) { }

        /// <summary>
        /// Default Constructor for generating random number collection.
        /// Will generate random numbers, minimum number given through maximum number given inclusive, with a maximum number of duplicates given.
        /// </summary>
        /// <param name="qtyofRandomNumbers">Quantity of random numbers wanted</param>
        /// <param name="minimumRandomNumber">Minimum number for random numbers</param>
        /// <param name="maximumRandomNumber">Maximum number for random numbers</param>
        /// <param name="maximumNumberOfDuplicates">Maximum number of duplicate numbers</param>
        public GenerateRandomNumbers(int qtyofRandomNumbers, int minimumRandomNumber, int maximumRandomNumber, int maximumNumberOfDuplicates) {
            if (maximumRandomNumber < minimumRandomNumber) { throw new ArgumentOutOfRangeException("maximumRandomNumber", "maximumRandomNumber can not be less than minimumRandomNumber"); }
            if (((maximumRandomNumber - minimumRandomNumber + 1) * maximumNumberOfDuplicates) < qtyofRandomNumbers) { throw new ArgumentOutOfRangeException("maximumNumberOfDuplicates", "Increase the maximumNumberOfDuplicates, inorder to generate the correct qtyofRandomNumbers"); }

            this.QuantityOfRandomNumbers = qtyofRandomNumbers;
            this.MinimumRandomNumber = minimumRandomNumber;
            this.MaximumRandomNumber = maximumRandomNumber;
            this.MaximumNumberOfDuplicates = maximumNumberOfDuplicates;
            this.RandomNumbers = new List<int>();
        }

        /// <summary>
        /// Generates a collection of random integers
        /// </summary>
        /// <returns>A List of random integers</returns>
        public List<int> GenerateRandomNumberCollection() {
            var randomNum = new Random();

            while (RandomNumbers.Count < this.QuantityOfRandomNumbers) {
                var rndNum = randomNum.Next(this.MinimumRandomNumber, this.MaximumRandomNumber + 1);
                var duplicateCount = this.RandomNumbers.FindAll(s => s.Equals(rndNum));

                if (duplicateCount.Count < this.MaximumNumberOfDuplicates) {
                    this.RandomNumbers.Add(rndNum);
                }
            }

            return RandomNumbers;
        }
        
    }
}
