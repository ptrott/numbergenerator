﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NumberGenerator {
    /// <summary>
    /// Interface for generating random numbers
    /// </summary>
    public interface IRandomNumberGenerator {

        /// <summary>
        /// Generate Random Number Collection
        /// </summary>
        /// <returns>List of random integers</returns>
        List<int> GenerateRandomNumberCollection();
    }
}
