﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NumberGenerator {
    /// <summary>
    /// Fake NumbersForGame class for Testing.
    /// </summary>
    public class FakeGenerateRandomNumbers : IRandomNumberGenerator {

        /// <summary>
        /// Default Constructor
        /// </summary>
        public FakeGenerateRandomNumbers() {
        }

        /// <summary>
        /// Generates a list of integer numbers.
        /// </summary>
        /// <returns>Returns a List of integer numbers.</returns>
        public List<int> GenerateRandomNumberCollection() {
            return new List<int>() { 1, 1, 2, 3, 4, 2, 2, 5, 6, 7, 8 };
        }
    }
}
